# EJERCICIO 9.3. MINIPRÁCTICA 2

from .models import Contenido
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.template import loader


@csrf_exempt
def index(request):
    if request.method == "POST":
        url = request.POST['url']
        short = request.POST['short']
        if not url.startswith('http://') and not url.startswith('https://'):
            url = "https://" + url
        try:
            c = Contenido.objects.get(short=short)
            c.delete()
        except Contenido.DoesNotExist:
            pass
        c = Contenido(short=short, url=url)
        c.save()
    content_list = Contenido.objects.all()
    template = loader.get_template('acorta/index.html')
    context = {'content_list': content_list}
    return HttpResponse(template.render(context, request))


def get_content(request, short):
    clave = short
    try:
        contenido = Contenido.objects.get(short=clave)
        template = loader.get_template('acorta/redirect.html')
        context = {'content_list': contenido}
        return HttpResponse(template.render(context, request))
    except Contenido.DoesNotExist:
        return HttpResponse("Ha introducido la url acortada " + short + " la cual no está en la base de datos.", status=404)

